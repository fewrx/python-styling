# python-styling

My personal python styling rules & guidelines

## Requirements

For the Visual Studio Code settings, you must have vscode.

You must also have the Python vscode extension installed.

> A great VSCode python linting guide can be found [here](https://code.visualstudio.com/docs/python/linting)

The linters used are:

- `flake8`
- `pylint`

`flake8-docstrings`, the `flake8` plugin, is also used.

### flake8

`pip install flake8`  

### pylint

`pip install pylint`  

### flake8-docstrings

`pip install flake8-docstrings`

## Configuration

### VSCode user or workspace settings

```json
  "python.linting.enabled": true,
  "python.linting.flake8Enabled": true,
  "python.linting.pylintEnabled": true,
  "python.linting.pydocstyleEnabled": false,
  "python.linting.flake8Args": ["--ignore=D1", "--docstring-convention=pep257"]
```

I have these in my user settings.json, but they can be applied to only specific workspaces as well.

The arguments passed to `flake8` are both used by the `flake8-docstrings` plugin. If you are not using the `flake8-docstrings` plugin, instead enable `pydocstyle` and remove the `flake8-docstrings` arguments.

Error types starting in D1 are warnings about missing docstrings for a function, class, etc. As I include docstrings where necessary, but not in every script I write, I choose to disable these messages so `flake8` isn't constantly bothering about a docstring for `main()`.

Specifying the docstring convention passes an argument to the `flake8-docstrings` plugin, detailing what docstring convention to apply when linting. I choose `pep257` as it's the most globally recognized, and the style I am most comfortable with. The PEP 257 docstring conventions can be found [here](https://www.python.org/dev/peps/pep-0257/).

### Additional non-linter settings

```json
  "autoDocstring.docstringFormat": "sphinx",
  "[python]": {
    "editor.rulers": [
      79
    ]
  }
```

The first setting is from a vscode extension `autoDocstring`, called "Python Docstring Generator" in the Marketplace. It creates a template from the function arguments and type declarations already present, and saves a ton of time when creating docstrings for existing functions. As the extension doesn't have PEP 257 as an option (yet), I am instead using `sphinx`, as it is similar.

`editor.rulers` simply shows a vertical line ([ruler](https://stackoverflow.com/questions/29968499/vertical-rulers-in-visual-studio-code)) in the vscode window, after column 79 (the [maximum allowed line length in PEP8](https://www.python.org/dev/peps/pep-0008/#maximum-line-length)). The `[python]` block only shows this ruler when a python file is being edited, and hides it for all other language modes.
